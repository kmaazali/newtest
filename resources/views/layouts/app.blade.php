<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">
    <title>@yield('title') - Amaxza Digital</title>

    <meta name="keywords" content="Amaxza Digital - Listtee" />
    <meta name="description" content="One Stop Solution For Your Amazon Listings">
    <meta name="author" content="Khawaja Maaz Ali">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{url('assets/vendor/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{url('assets/vendor/font-awesome/css/font-awesome.css')}}" />
    <link rel="stylesheet" href="{{url('assets/vendor/magnific-popup/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{url('assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{url('assets/stylesheets/theme.css')}}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{url('assets/stylesheets/skins/default.css')}}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{url('assets/stylesheets/theme-custom.css')}}">

    <!-- Head Libs -->
    <script src="{{url('assets/vendor/modernizr/modernizr.js')}}"></script>

</head>

<body>
@yield('content')
<!-- Vendor -->
<script src="{{url('assets/vendor/jquery/jquery.js')}}"></script>
<script src="{{url('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
<script src="{{url('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{url('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
<script src="{{url('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
<script src="{{url('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{url('assets/javascripts/theme.js')}}"></script>

<!-- Theme Custom -->
<script src="{{url('assets/javascripts/theme.custom.js')}}"></script>

<!-- Theme Initialization Files -->
<script src="{{url('assets/javascripts/theme.init.js')}}"></script>
</body>

</html>
